Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Unhide
Upstream-Contact: Yago Jesus <yjesus@security-projects.com>
Source: http://www.unhide-forensics.info/

Files: *
Copyright: 2005-2021 Yago Jesus <yjesus@security-projects.com>
           2010-2021 Patrick Gouin
License: GPL-3+

Files: debian/*
Copyright: 2007-2009 Francois Marier <francois@debian.org>
           2009      Daniel Baumann <daniel@debian.org>
           2010-2011 Christophe Monniez <christophe.monniez@fccu.be>
           2011-2013 Julien Valroff <julien@debian.org>
           2015      Giovani Augusto Ferreira <giovani@riseup.net>
           2016-2018 Raphaël Hertzog <hertzog@debian.org>
           2016-2022 Samuel Henrique <samueloph@debian.org>
           2018-2019 Joao Eriberto Mota Filho <eriberto@debian.org>
           2019      Thiago Andrade Marques <thmarques@gmail.com>
           2015      Eriberto Mota <eriberto@cepheus.cdciber.eb.mil.br>
           2022      Fukui Daichi <a.dog.will.talk@akane.waseda.jp>
           2010      Michael Prokop <mika@debian.org>
License: GPL-3+

Files: ToolTip.py
Copyright: 2009 Tucker Beck
License: Expat

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public License
 can be found in /usr/share/common-licenses/GPL-3 file.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy 
 of this software and associated documentation files (the “Software”), to deal 
 in the Software without restriction, including without limitation the rights 
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 copies of the Software, and to permit persons to whom the Software is 
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in 
 all copies or substantial portions of the Software.
 . 
 THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 THE SOFTWARE.
